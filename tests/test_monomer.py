from dataclasses import dataclass, field
from typing import Text

from monomer import MonomerDatabase
from monomer.compat import cdk


@dataclass
class Driver(object):
    driver_id: int
    name: Text = field(default=None)
    age: int = field(default=None)


@dataclass
class Car(object):
    Id: int
    Name: Text
    Price: int
    driver: Driver = field(default=None)


def test_functional():
    s = cdk.Stack()
    db = MonomerDatabase(s, 'Test', database_name='autobahn', force_bucket=True)
    driver_table = db.add_table(Driver)
    assert db.tables["driver"] == driver_table
    assert list(c.name for c in driver_table.columns) == ["driver_id", "name", "age"]
