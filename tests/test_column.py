from dataclasses import field
from typing import Text

from monomer import Column
from monomer.compat import glue

def test_from_field():
    dt_field = field()
    dt_field.name = "test_field"
    dt_field.type = Text
    c = Column.from_field(dt_field)
    assert c.type == glue.Schema.STRING

