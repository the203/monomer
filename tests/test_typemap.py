import typing

from monomer.compat import glue, array
from monomer.typemap import python_to_glue


def test_simple_translation():
    assert python_to_glue(typing.Text) == glue.Schema.STRING
    assert python_to_glue(type("A String")) == glue.Schema.STRING
    assert python_to_glue(type(3)) == glue.Schema.INTEGER
    assert python_to_glue(int) == glue.Schema.INTEGER
    assert python_to_glue(type(4.4)) == glue.Schema.FLOAT


def test_complex_translation():
    assert (python_to_glue(typing.List[typing.Text]) ==
            glue.Type(input_string="array<string>", is_primitive=False))

    assert (python_to_glue(typing.Dict[typing.Text, int]) ==
            glue.Type(input_string="map<string,int>", is_primitive=False))

    assert (python_to_glue(typing.Dict[typing.Text, typing.List[typing.Text]]) ==
            glue.Type(input_string="map<string,array<string>>", is_primitive=False))


def test_array():
    a = array(glue.Schema.FLOAT)
    assert a.input_string == "array<float>"
