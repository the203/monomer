.. Monomer documentation master file, created by
   sphinx-quickstart on Mon Dec 21 10:15:01 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Monomer
=======
Welcome to Monomer's documentation. Monomer was born out of an experimental plugin
for SqlAlchemy to have a more nature support for dataclasses. While the implementations
share virtually no commonality technically the concept between the 2 is shared. Dataclasses
provide a wonderful way to model data structures in python but Glue catelogs (through AWS's CDK)
and SqlAlchemy mappers require almost 100% duplicative effort to model the data for the respective
library. By using Monomer an application's model will be defined in the application and merly
referenced on the application's CDK stack.


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
