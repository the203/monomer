from collections import UserDict
from dataclasses import fields as dtc_fields, asdict as dtc_asdict, field, is_dataclass

import boto3
import datetime
import inspect
import logging
import re
from itertools import chain
from typing import Text, Dict

from .column import Column
from .compat import cdk, glue, s3, iam

logger = logging.getLogger("monomer")
try:
    from sphinx.pycode.parser import Parser as CodeParser
except ImportError:
    logger.info("Sphinx Parser unavailable, will not add doc strings to table comments")


    class CodeParser(object):

        def __init__(self, code: str, encoding: str = 'utf-8') -> None:
            self.code = code
            self.encoding = encoding
            self.annotations = {}
            self.comments = {}
            self.deforders = {}
            self.definitions = {}
            self.finals = []
            self.overloads = {}

        def parse(self):
            pass


class DocParser(CodeParser):

    def __init__(self, datacls) -> None:
        code = inspect.getsource(datacls)
        self.name = datacls.__name__
        super().__init__(code)
        self.parse()

    def get_doc(self, name):
        try:
            comment = self.comments.get((self.name, name))
            return self.sanitize(comment)

        except AttributeError:
            return None

    @staticmethod
    def sanitize(docstring, max_len=255):
        docstring = docstring.split("..")[0]
        docstring = docstring.strip()[:max_len].replace("\n", " ")
        docstring = re.sub(r':[^:]{,6}:', "", docstring)
        return docstring


class monomer_property(property):

    def __init__(self, prp_or_column):
        self._column = None
        if isinstance(prp_or_column, Column):
            self._column = prp_or_column
        else:
            super().__init__(prp_or_column)

    def __call__(self, prp, *args, **kwargs):
        return super().__init__(prp)

    @staticmethod
    def persisted_properties(datacls):
        if not isinstance(datacls, type):
            datacls = type(datacls)
        for k, v in inspect.getmembers(datacls, lambda a: isinstance(a, monomer_property)):
            yield v.make_field(k)

    def make_field(self, k):
        f = field()
        f.name = k
        _type = inspect.signature(self.fget).return_annotation
        if _type is inspect.Signature.empty:
            _type = Text
        f.type = _type
        if self._column is not None:
            f.metadata["monomer"] = {"column": self._column}
        return f


class MonomerDatabase(cdk.Construct):
    """Monomer's main AWS CDK construct. Each Glue Database should have its own construct. By default an S3 bucket 
    is created for each database but this behavior can be overridden simply by setting the databases s3 bucket 
    after initialization: 
    
    ... code: 
        db = MonomerDatabase(stack, 'someid', 'our_data')
        db.s3bucket = stack.predefined_shared_s3 
    
    
    """

    @staticmethod
    def READ(t):
        return t.grant_read

    @staticmethod
    def WRITE(t):
        return t.grant_write

    def __init__(self, scope: cdk.Construct, id: Text, database_name: Text, bucket_name=None, force_bucket=False):
        super().__init__(scope, id)
        database_name = database_name.lower()
        self.db = glue.Database(self, database_name, database_name=database_name)
        bucket_name = (bucket_name or f"{database_name}-data-storage").lower()
        self.bucket = self.make_bucket(bucket_name, force=force_bucket)
        self.tables: Dict[Text, glue.Table] = {}

    def make_bucket(self, bucket_name, force=False):
        if not force and boto3.resource('s3').Bucket(bucket_name).creation_date:
            return s3.Bucket.from_bucket_name(self, f"{bucket_name}-store", bucket_name=bucket_name)
        else:
            return s3.Bucket(self, f"{bucket_name}-store",
                             bucket_name=bucket_name,
                             block_public_access=s3.BlockPublicAccess.BLOCK_ALL,
                             encryption=s3.BucketEncryption.S3_MANAGED)

    def add_module(self, mod, ignore=None):
        ignore = ignore or []
        for k, v in mod.__dict__.items():
            if k in ignore:
                continue
            if k[0:2] != "__" and is_dataclass(v):
                self.add_table(v)

    def add_table(self, datacls, bucket: s3.Bucket = None):
        """ Add table schemas based on dataclasses defined in the application. 
        :bucket: override the database's setting for s3 location. If a bucket location is provided
        monomer assumes the entire path is specified here and will send a prefix of blank.
        """
        dt_name = datacls.__name__
        docs = DocParser(datacls)
        columns = list(Column.from_field(dt_field, cls_docs=docs) for dt_field in fields(datacls))
        table = glue.Table(self, f'{dt_name}Table',
                           description=DocParser.sanitize(datacls.__doc__),
                           database=self.db,
                           table_name=dt_name.lower(),
                           bucket=bucket or self.bucket,
                           s3_prefix=dt_name if bucket is None else '',
                           columns=list(col.to_glue() for col in columns),
                           data_format=glue.DataFormat.CSV)
        self.tables[dt_name.lower()] = table

        return table

    def grant(self, role: iam.Role, *grants, table_filter=None):
        if table_filter is None:
            table_filter = lambda t: True
        for t in self.tables.values():
            if table_filter(t):
                for grant in grants:
                    grant(t)(role)


def fields(datacls):
    return chain(dtc_fields(datacls), monomer_property.persisted_properties(datacls))


_Empty = "TOTALLY_EMPTY"


def asdict(datacls, dict_factory=_Empty):
    if dict_factory == _Empty:
        dict_factory = HiveSafeDict
    if dict_factory is None:
        dict_factory = dict
    dtc_dict = dtc_asdict(datacls, dict_factory=dict_factory)
    for f in monomer_property.persisted_properties(datacls.__class__):
        dtc_dict[f.name] = getattr(datacls, f.name)
    return dtc_dict


class HiveSafeDict(UserDict):
    def __setitem__(self, key, item) -> None:
        if isinstance(item, datetime.datetime):
            item = int(item.timestamp() * 1000)
        super().__setitem__(key, item)
